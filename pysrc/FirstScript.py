
#!/usr/bin/python3.8

# import dependencies
import sys
sys.path.append('/home/jeanlinux/Documents/codes/GITLAB/secondpy/pymodule/')

import getopt
from pathlib import Path
from UseCrypto import py_crypto_get_random_bytes, py_crypto_use_rsa_cipher, py_crypto_use_rsa_decipher, py_crypto_use_aes_cipher, py_crypto_use_aes_decipher

# some global variables
SOME_MIN_CONSTANTE_VALUE = 0
SOME_MAX_CONSTANTE_VALUE = 9999
SOME_CONSTANTE_VALUE = 5
original_aes_datafile = ''
ciphered_aes_datafile = ''
unciphered_aes_datafile = ''
aes_key_file = ''
iv_key_file = ''
original_rsa_datafile = ''
ciphered_rsa_datafile = ''
unciphered_rsa_datafile = ''
pub_key_file = ''
priv_key_file = ''
input_files_list = list()
some_directory_name = ''
input_dirs_list = list()
some_integer_value = 0
some_array_variable = [0] * SOME_CONSTANTE_VALUE
debug_lvl = 0

# function : main argument parser
def py_main_parser(argv):
    global original_aes_datafile
    global ciphered_aes_datafile
    global unciphered_aes_datafile
    global aes_key_file
    global iv_key_file
    global original_rsa_datafile
    global ciphered_rsa_datafile
    global unciphered_rsa_datafile
    global pub_key_file
    global priv_key_file
    global input_files_list
    global some_directory_name
    global some_integer_value
    global some_array_variable
    global debug_level

    # lets use getopt to parse the argv list
    # short options : hd:f:r:n:
    # since h doesn't take a value in option, it doesn't need a semicolomn
    # d option : debug level
    # f option : file name
    # r option : directory name
    # n option : integer value
    options, remainder = getopt.getopt(argv,'hd:a:b:c:e:f:g:i:j:p:s:r:n:' ,['help',
                                                                            'debugl=',
                                                                            'aesoriginal=',
                                                                            'aesciphered=',
                                                                            'aesunciphered=',
                                                                            'aesks=',
                                                                            'aesiv=',
                                                                            'rsaoriginal=',
                                                                            'rsaciphered=',
                                                                            'rsacdeiphered=',
                                                                            'pubkey=',
                                                                            'privkey=',
                                                                            'dirname=',
                                                                            'number='])

    for opt, arg in options:
        if opt in ('-h','--help'):
            py_main_help()
            sys.exit(2)
        elif opt in ('-d','--debugl'):
            debug_level = int(arg)
        elif opt in ('-a', '--aesoriginal'):
            original_aes_datafile = arg
            input_files_list.append(original_aes_datafile)
        elif opt in ('-b', '--aesciphered'):
            input_files_list.append(ciphered_aes_datafile)
        elif opt in ('-c', '--aesunciphered'):
            input_files_list.append(unciphered_aes_datafile)
        elif opt in ('-e', '--aesks'):
            aes_key_file = arg
            input_files_list.append(aes_key_file)
        elif opt in ('-f', '--aesiv'):
            iv_key_file = arg
            input_files_list.append(iv_key_file)
        elif opt in ('-g', '--rsaoriginal'):
            original_rsa_datafile = arg
            input_files_list.append(original_rsa_datafile)
        elif opt in ('-i', '--rsaciphered '):
            ciphered_rsa_datafile = arg
            input_files_list.append(ciphered_rsa_datafile)
        elif opt in ('-j', '--rsacdeiphered'):
            unciphered_rsa_datafile = arg
            input_files_list.append(unciphered_rsa_datafile)
        elif opt in ('-p','--pubkey'):
            pub_key_file = arg
            input_files_list.append(pub_key_file)
        elif opt in ('-s','--privkey'):
            priv_key_file = arg
            input_files_list.append(priv_key_file)
        elif opt in ('-r','--dirname'):
            some_directory_name = arg
        elif opt in ('-n','--number'):
            some_integer_value = int(arg)

    if (debug_level > 0):
        print("[DBG>0]  debug_level: %d" %debug_level)
        print("[DBG>0]  original_aes_datafile: %s" %original_aes_datafile)
        print("[DBG>0]  ciphered_aes_datafile: %s" %ciphered_aes_datafile)
        print("[DBG>0]  unciphered_aes_datafile: %s" %unciphered_aes_datafile)
        print("[DBG>0]  aes_key_file: %s" %aes_key_file)
        print("[DBG>0]  iv_key_file: %s" %iv_key_file)
        print("[DBG>0]  original_rsa_datafile: %s" %original_rsa_datafile)
        print("[DBG>0]  ciphered_rsa_datafile: %s" %ciphered_rsa_datafile)
        print("[DBG>0]  unciphered_rsa_datafile: %s" %unciphered_rsa_datafile)
        print("[DBG>0]  pub_key_file: %s" %pub_key_file)
        print("[DBG>0]  priv_key_file: %s" %priv_key_file)
        print("[DBG>0]  some_directory_name: %s" %some_directory_name)
        print("[DBG>0]  some_integer_value: %d" %some_integer_value)

    if (debug_level > 2):
        print("[DBG>2]	ARGV      :%s" %argv)
        print("[DBG>2]	OPTIONS   :%s" %options)
        print("[DBG>2]	REMAINING :%s" %remainder)

# function print
def py_main_help():
	print("------- AymenLA ------")
	print("")
	print("	py code exemple for test and tutorial")
	print("	usage :")
	print("	")
	print("	script_name.py [-f][--filename=] filename [-p][--pubkey=] pubkey [-s][--privkey=] privkey [-r][--dirname=] directoryname [-n][--number=] some_number [-d][--debugl=] any_debug_level")
	print("	-a | --aesoriginal : input filename for AES cipher")
	print("	-b | --aesciphered : output filename for AES cipher")
	print("	-c | --aesunciphered : output filename for AES decipher")
	print("	-e | --aesks : secret key filename for AES cipher")
	print("	-f | --aesiv : initial vector filename for AES cipher")
	print("	-p | --pubkey : input public key")
	print("	-s | --privkey : input private key")
	print("	-g | --rsaoriginal : input filename for RSA cipher")
	print("	-i | --rsaciphered : output filename for RSA cipher")
	print("	-j | --rsacdeiphered : output filename for RSA decipher")
	print("	-r | --dirname : input directory")
	print("	-n | --number : some integer")
	print("	-d | --debugl : debug level from 0 (no debug) to 3 (very chaty debug)")
	print("	-h | --help : print this paragraph")
	print("	")
	print("	a-laouini.com - Personal Blog")
	print("	Aymen LAOUINI : Electronics / Embedded SW engineer")
	print("	France")
	print("")

# function
def py_verify_input_elements():
    global original_aes_datafile
    global ciphered_aes_datafile
    global unciphered_aes_datafile
    global aes_key_file
    global iv_key_file
    global original_rsa_datafile
    global ciphered_rsa_datafile
    global unciphered_rsa_datafile
    global pub_key_file
    global priv_key_file
    global input_files_list
    global some_directory_name
    global some_integer_value
    global some_array_variable
    global debug_level

    # -f option : fileinput, does it exist ?

    for single_elem in input_files_list:
        any_file = Path(single_elem)
        if any_file.is_file():
            if (debug_level > 0):
                print("[DBG>0] %s is a valid file" %single_elem)
        else:
            if (debug_level > 0):
                print("[DBG>0] %s in not a valid file" %single_elem)
                sys.exit(2)


    # -r option : directory input, does it exist ?
    any_dir = Path(some_directory_name)
    if any_dir.is_dir():
        if (debug_level > 0):
            print("[DBG>0] %s in a valid directory" %some_directory_name)
    else:
        if (debug_level > 0):
            print("[DBG>0] %s in not a valid directory" %some_directory_name)
        sys.exit(2)

    # -n option : number input, is it valid ?
    if some_integer_value < SOME_MIN_CONSTANTE_VALUE:
        print("[DBG>0] %d in not a valid integer" %some_integer_value)
        sys.exit(2)
    elif some_integer_value > SOME_MAX_CONSTANTE_VALUE:
        if (debug_level > 0):
            print("[DBG>0] %d in not a valid integer" %some_integer_value)
            sys.exit(2)
    else:
        if (debug_level > 0):
            print("[DBG>0] %d in a valid integer" %some_integer_value)
# excuting main
if __name__ == "__main__":
	# parse input args
    py_main_parser(sys.argv[1:])

    # verify inputs
    py_verify_input_elements()

    # carbadge code : just some crypto test
    # RSA cipher test
    rsa_ciphered_text = py_crypto_use_rsa_cipher(pub_key_file, b"AymenLA test RSA2048 RANDOM_PADDING")
    print ("[DBG>0] __main__: RSA ciphered text %s " %rsa_ciphered_text)
    print ("[DBG>0] __main__: RSA deciphered text %s " %py_crypto_use_rsa_decipher(priv_key_file, rsa_ciphered_text))

    # AES cipher test
    ks_key = py_crypto_get_random_bytes(24)
    iv_bin = py_crypto_get_random_bytes(16)
    aes_ciphered_text = py_crypto_use_aes_cipher(ks_key, iv_bin, b"a secret message")
    print ("[DBG>0] __main__: AES ciphered text %s " %aes_ciphered_text)
    aes_clear_text = py_crypto_use_aes_decipher(ks_key, iv_bin, aes_ciphered_text)
    print ("[DBG>0] __main__: AES deciphered text %s " %aes_clear_text)
