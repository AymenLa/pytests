# !/usr/bin/python3.8

import os

# import sys, use system path appending to add ald python modules
import sys
sys.path.append('/usr/lib/python2.7/dist-packages')

# import crypto elements
# if not installed, use : pip install cryptography
import cryptography
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.asymmetric import rsa, padding
from cryptography.hazmat.primitives import serialization, hashes
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes


random_bytes = ''
public_key_inst = 0
private_key_inst = 0
random_iv = ''
random_ks = ''

# function :load a private key
def py_crypto_load_private_key(PEM_FILE):
    global private_key_inst
    if PEM_FILE != '':
        with open(PEM_FILE, "rb") as key_file:
            private_key_inst = serialization.load_pem_private_key(
                key_file.read(),
                password=None,
                backend=default_backend()
            )
            print("[DBG] py_crypto_load_private_key: private key %s loaded" %PEM_FILE)
            print("[DBG] py_crypto_load_private_key: load_pem_private_key result = %s" %private_key_inst)
            return private_key_inst
    else:
        print("[DBG] py_crypto_load_private_key: invalid input private key")
        sys.exit(1)

# function :load a public key
def py_crypto_load_public_key(PEM_FILE):
    global public_key_inst
    if PEM_FILE != '':
        with open(PEM_FILE, "rb") as key_file:
            public_key_inst = serialization.load_pem_public_key(
                key_file.read(),
                backend=default_backend()
            )
            print("[DBG] py_crypto_load_public_key: public key %s loaded" %PEM_FILE)
            print("[DBG] py_crypto_load_public_key: load_pem_public_key result = %s" %public_key_inst)
            return public_key_inst
    else:
        print("[DBG] py_crypto_load_public_key: invalid input public key")
        sys.exit(1)

# function : get random bits
def py_crypto_get_random_bytes(RANDOM_NUM):
    global random_bytes
    if (RANDOM_NUM == 0):
        print("[DBG] py_crypto_get_random_bytes: invalid input RANDOM_NUM")
        sys.exit(2)
    elif (RANDOM_NUM == ''):
        print("[DBG] py_crypto_get_random_bytes: invalid input RANDOM_NUM")
        sys.exit(1)
    else:
        random_bytes = os.urandom(RANDOM_NUM)
        print("[DBG] py_crypto_get_random_bytes: generated %d random bytes" %RANDOM_NUM)
        print("[DBG] py_crypto_get_random_bytes: %s " %random_bytes)
        return random_bytes

# function cipher with RSA
def py_crypto_use_rsa_cipher(PUBKEY_PEM, CLEAR_MSG):
    public_key_local_inst = py_crypto_load_public_key(PUBKEY_PEM)
    if(len(CLEAR_MSG) >= 256 - 11):
        print("[DBG] py_crypto_use_rsa_cipher: ciphering error : input message too big")
        sys.exit(1)
    else:
        message_local = CLEAR_MSG
        algorithme_local = hashes.SHA256()
        mfg_local = padding.MGF1(algorithme_local)
        label_local = None
        print("[DBG] py_crypto_use_rsa_cipher: input data")
        print("[DBG] py_crypto_use_rsa_cipher: pub key = %s" %PUBKEY_PEM)
        print("[DBG] py_crypto_use_rsa_cipher: input text = %s" %message_local)
        print("[DBG] py_crypto_use_rsa_cipher: input text lenght = %s" %len(message_local))
        padding_local = padding.OAEP(mfg_local,
                                     algorithme_local,
                                     label_local)

        ciphered_text_local = public_key_local_inst.encrypt( message_local,
                                                    padding_local)
        print("[DBG] py_crypto_use_rsa_cipher: output ciphered text = %s" %ciphered_text_local)
        return ciphered_text_local

# function decipher with RSA
def py_crypto_use_rsa_decipher(PRIVKEY_PEM, CIPHERED_MSG):
    if(len(CIPHERED_MSG) != 256):
        print("[DBG] py_crypto_use_rsa_decipher: input data ERROR")
        sys.exit(1)
    else:
        private_key_local_inst = py_crypto_load_private_key(PRIVKEY_PEM)
        ciphered_message_local = CIPHERED_MSG
        algorithme_local = hashes.SHA256()
        print("[DBG] py_crypto_use_rsa_decipher: input data")
        print("[DBG] py_crypto_use_rsa_decipher: priv key = %s" %PRIVKEY_PEM)
        print("[DBG] py_crypto_use_rsa_decipher: input text = %s" %ciphered_message_local)
        print("[DBG] py_crypto_use_rsa_decipher: input text lenght = %s" %len(ciphered_message_local))
        mfg_local = padding.MGF1(algorithme_local)
        label_local = None
        padding_local = padding.OAEP(mfg_local,
                                     algorithme_local,
                                     label_local)
        plain_text_local = private_key_local_inst.decrypt( ciphered_message_local,
                                                    padding_local)
        print("[DBG] py_crypto_use_rsa_cipher: output clear text = %s" %plain_text_local)
        return plain_text_local

# function cipher with AES
def py_crypto_use_aes_cipher(SECRET_KEY, SECRET_IV, CLEAR_MESSAGE):
    backend_local = default_backend()
    clear_message_local = CLEAR_MESSAGE
    key_local = SECRET_KEY
    iv_local = SECRET_IV
    print("[DBG] py_crypto_use_aes_cipher: input data")
    print("[DBG] py_crypto_use_aes_cipher: key = %s" %key_local)
    print("[DBG] py_crypto_use_aes_cipher: init vector = %s" %iv_local)
    print("[DBG] py_crypto_use_aes_cipher: input text = %s" %clear_message_local)
    print("[DBG] py_crypto_use_aes_cipher: input text lenght = %s" %len(clear_message_local))
    mode_local = modes.CBC(iv_local)
    algorithme_local = algorithms.AES(key_local)
    cipher_local = Cipher(algorithme_local,
                          mode_local,
                          backend_local)
    encryptor_local = cipher_local.encryptor()
    ciphered_message_local = encryptor_local.update(clear_message_local) + encryptor_local.finalize()
    print("[DBG] py_crypto_use_aes_cipher: output ciphered text = %s" %ciphered_message_local)
    return ciphered_message_local


# function decipher with AES
def py_crypto_use_aes_decipher(SECRET_KEY, SECRET_IV, CIPHERED_MESSAGE):
    backend_local = default_backend()
    ciphered_message_local = CIPHERED_MESSAGE
    key_local = SECRET_KEY
    iv_local = SECRET_IV
    print("[DBG] py_crypto_use_aes_decipher: input data")
    print("[DBG] py_crypto_use_aes_decipher: key = %s" %key_local)
    print("[DBG] py_crypto_use_aes_decipher: init vector = %s" %iv_local)
    print("[DBG] py_crypto_use_aes_decipher: input ciphered message = %s" %ciphered_message_local)
    print("[DBG] py_crypto_use_aes_decipher: input ciphered lenght = %s" %len(ciphered_message_local))
    mode_local = modes.CBC(iv_local)
    algorithme_local = algorithms.AES(key_local)
    cipher_local = Cipher(algorithme_local,
                          mode_local,
                          backend_local)
    decryptor_local = cipher_local.decryptor()
    clear_message_local = decryptor_local.update(ciphered_message_local) + decryptor_local.finalize()
    print("[DBG] py_crypto_use_aes_decipher: output clear text = %s" %clear_message_local)
    return clear_message_local
